#!/bin/sh

### If unspecified, the hostname of the container is taken as the JobManager address
JOB_MANAGER_RPC_ADDRESS=${JOB_MANAGER_RPC_ADDRESS:-$(hostname -f)}
###

echo "Starting Job Manager"
sed -i -e "s/jobmanager.rpc.address: localhost/jobmanager.rpc.address: ${JOB_MANAGER_RPC_ADDRESS}/g" $FLINK_HOME/conf/flink-conf.yaml

echo "config file: " && grep '^[^\n#]' $FLINK_HOME/conf/flink-conf.yaml
#exec $FLINK_HOME/bin/jobmanager.sh start-foreground cluster
exec $FLINK_HOME/bin/jobmanager.sh start-foreground cluster

#exec "$@"
